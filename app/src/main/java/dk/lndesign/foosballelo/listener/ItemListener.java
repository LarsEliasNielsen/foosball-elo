package dk.lndesign.foosballelo.listener;

/**
 * Listener used when item in player and game list are clicked.
 * Callback will hold clicked player or game id.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public interface ItemListener {
    /**
     * Called when an item is clicked.
     * @param itemId Id of clicked player or game.
     */
    void itemClicked(int itemId);
}
