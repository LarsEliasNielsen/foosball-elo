package dk.lndesign.foosballelo.listener;

/**
 * Listener used when interacting with dialog fragments.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public interface DialogListener {
    void itemCreated();
}
