package dk.lndesign.foosballelo.fragment;

import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import dk.lndesign.foosballelo.R;
import dk.lndesign.foosballelo.adapter.PlayerSpinnerAdapter;
import dk.lndesign.foosballelo.dao.GameSource;
import dk.lndesign.foosballelo.dao.PlayerGameSource;
import dk.lndesign.foosballelo.dao.PlayerSource;
import dk.lndesign.foosballelo.listener.DialogListener;
import dk.lndesign.foosballelo.model.Game;
import dk.lndesign.foosballelo.model.Player;
import dk.lndesign.foosballelo.model.Team;
import dk.lndesign.foosballelo.util.EloUtil;

/**
 * Dialog fragment to create and edit games.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class GameEntityDialog extends DialogFragment {

    private final String TAG = this.getClass().getSimpleName();

    private Context mContext;
    // For creating a complete game, we must use all sources.
    private GameSource mGameSource;
    private PlayerGameSource mPlayerGameSource;
    private PlayerSource mPlayerSource;

    DialogListener mListener;

    public GameEntityDialog() {}

    public static GameEntityDialog newInstance() {
        return new GameEntityDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_game_entity, container);
        getDialog().setTitle(getResources().getString(R.string.create_game));

        mContext = inflater.getContext();

        mGameSource = new GameSource(mContext);
        mPlayerGameSource = new PlayerGameSource(mContext);
        mPlayerSource = new PlayerSource(mContext);
        // Open sources.
        try {
            mGameSource.open();
            mPlayerGameSource.open();
            mPlayerSource.open();
        } catch (SQLException e) {
            Log.e(TAG, "onCreateView: Failed to open source(s)");
        }

        // Get players and create adapter.
        List<Player> players = mPlayerSource.getAllPlayers();
        ArrayAdapter<Player> adapter = new PlayerSpinnerAdapter(mContext, android.R.layout.simple_spinner_item, players);

        // Get spinners and set adapter.
        final Spinner playerRedSpinner1 = (Spinner) view.findViewById(R.id.player_red_spinner_1);
        final Spinner playerRedSpinner2 = (Spinner) view.findViewById(R.id.player_red_spinner_2);
        final Spinner playerBlueSpinner1 = (Spinner) view.findViewById(R.id.player_blue_spinner_1);
        final Spinner playerBlueSpinner2 = (Spinner) view.findViewById(R.id.player_blue_spinner_2);
        playerRedSpinner1.setAdapter(adapter);
        playerRedSpinner2.setAdapter(adapter);
        playerBlueSpinner1.setAdapter(adapter);
        playerBlueSpinner2.setAdapter(adapter);

        // Radio buttons.
        final RadioGroup winnerRadioGroup = (RadioGroup) view.findViewById(R.id.game_winner_radio);

        Button submitButton = (Button) view.findViewById(R.id.submit_button);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // List of players.
                // TODO: Check this cast.
                Player playerRed1 = (Player) playerRedSpinner1.getSelectedItem();
                Player playerRed2 = (Player) playerRedSpinner2.getSelectedItem();
                Player playerBlue1 = (Player) playerBlueSpinner1.getSelectedItem();
                Player playerBlue2 = (Player) playerBlueSpinner2.getSelectedItem();

                List<Player> playerList = new ArrayList<>();
                playerList.add(0, playerRed1);
                playerList.add(1, playerRed2);
                playerList.add(2, playerBlue1);
                playerList.add(3, playerBlue2);

                // Fields.
                int idx = winnerRadioGroup.getCheckedRadioButtonId();
                RadioButton radioButton = (RadioButton) winnerRadioGroup.findViewById(idx);
                String winningTeam = radioButton.getText().toString()
                        .equalsIgnoreCase(Team.RED) ? Team.RED : Team.BLUE;

                if (idx != -1 && isDifferentFromEach(playerList)) {

                    // Calculate game.
                    playerList = EloUtil.calculateGameScore(
                            playerList,
                            winningTeam);

                    if (playerList != null) {
                        // Insert game into database.
                        Game game = mGameSource.createGame(winningTeam);

                        // Insert player game relation into database.
                        mPlayerGameSource.createPlayerGame(playerList.get(0).getId(), game.getId(), Team.RED, playerList.get(0).getScoreAlteration());
                        mPlayerGameSource.createPlayerGame(playerList.get(1).getId(), game.getId(), Team.RED, playerList.get(1).getScoreAlteration());
                        mPlayerGameSource.createPlayerGame(playerList.get(2).getId(), game.getId(), Team.BLUE, playerList.get(2).getScoreAlteration());
                        mPlayerGameSource.createPlayerGame(playerList.get(3).getId(), game.getId(), Team.BLUE, playerList.get(3).getScoreAlteration());

                        // Adjust player scores.
                        for (Player player : playerList) {
                            mPlayerSource.adjustPlayerScore(player);
                        }
                    } else {
                        Log.e(TAG, "Something went wrong in Elo calculations.");
                    }

                    // Close dialog.
                    mListener.itemCreated();
                    getDialog().dismiss();
                } else {
                    Toast.makeText(mContext, "All players must be different, all field must be set", Toast.LENGTH_LONG).show();
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        // Open source.
        try {
            mGameSource.open();
            mPlayerGameSource.open();
            mPlayerSource.open();
        } catch (SQLException e) {
            Log.e(TAG, "onResume: Failed to open source(s)", e);
        }

        // Get screen size.
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        // Get width of dialog according to screen size.
        Window window = getDialog().getWindow();
        window.setLayout(size.x * 4 / 5, LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onPause() {
        super.onPause();

        mGameSource.close();
        mPlayerGameSource.close();
        mPlayerSource.close();
    }

    public void setListener(DialogListener listener) {
        this.mListener = listener;
    }

    /**
     * Check that all players in list are different from each other.
     * {@see http://stackoverflow.com/a/7604867/3965178}
     * TODO: Maybe this should be generic?
     *
     * @param players List of players.
     * @return True if all players are different from each other.
     */
    private boolean isDifferentFromEach(List<Player> players) {
        Set<Player> playerPool = new HashSet<>();
        for (Player player : players) {
            if (playerPool.contains(player)) {
                return false;
            } else {
                playerPool.add(player);
            }
        }
        return true;
    }
}
