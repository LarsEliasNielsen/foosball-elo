package dk.lndesign.foosballelo.fragment;

import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.sql.SQLException;

import dk.lndesign.foosballelo.R;
import dk.lndesign.foosballelo.dao.PlayerSource;
import dk.lndesign.foosballelo.listener.DialogListener;

/**
 * Dialog fragment to add and edit players.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class PlayerEntityDialog extends DialogFragment {

    private final String TAG = this.getClass().getSimpleName();

    private PlayerSource mPlayerSource;
    private Context mContext;

    private EditText mPlayerNameEditText;

    DialogListener mListener;

    public PlayerEntityDialog() {}

    public static PlayerEntityDialog newInstance() {
        return new PlayerEntityDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_player_entity, container);
        getDialog().setTitle(getResources().getString(R.string.create_player));

        mContext = inflater.getContext();

        mPlayerSource = new PlayerSource(inflater.getContext());
        // Open source.
        try {
            mPlayerSource.open();
        } catch (SQLException e) {
            Log.e(TAG, "onCreateView: Failed to open source", e);
        }

        mPlayerNameEditText = (EditText) view.findViewById(R.id.edittext_player_name);

        Button submitButton = (Button) view.findViewById(R.id.submit_button);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create player.
                mPlayerSource.createPlayer(mPlayerNameEditText.getText().toString().trim());

                // Close dialog.
                mListener.itemCreated();
                getDialog().dismiss();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        // Open source.
        try {
            mPlayerSource.open();
        } catch (SQLException e) {
            Log.e(TAG, "onResume: Failed to open source", e);
        }

        // Get screen size.
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        // Get width of dialog according to screen size.
        Window window = getDialog().getWindow();
        window.setLayout(size.x * 4 / 5, LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPlayerSource.close();
    }

    public void setListener(DialogListener listener) {
        this.mListener = listener;
    }

}
