package dk.lndesign.foosballelo.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dk.lndesign.foosballelo.db.DbContract;
import dk.lndesign.foosballelo.db.DbHelper;
import dk.lndesign.foosballelo.model.Player;

/**
 * Data access object for {@link dk.lndesign.foosballelo.model.Player}.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class PlayerSource {

    private static final String TAG = PlayerSource.class.getSimpleName();

    // Database fields.
    private SQLiteDatabase mDatabase;
    private DbHelper mDbHelper;
    private String[] mColumnList = {
            DbContract.PlayerTable._ID,
            DbContract.PlayerTable.COLUMN_NAME_PLAYER_NAME,
            DbContract.PlayerTable.COLUMN_NAME_PLAYER_SCORE,
            DbContract.PlayerTable.COLUMN_NAME_MODIFIED
    };

    public PlayerSource(Context context) {
        mDbHelper = new DbHelper(context);
    }

    public void open() throws SQLException {
        mDatabase = mDbHelper.getWritableDatabase();
    }

    public void close() {
        mDbHelper.close();
    }

    /**
     * Create a new player in database and fetch player to model.
     * SQLite cursor query():
     * http://developer.android.com/reference/android/database/sqlite/SQLiteDatabase.html#query(
     * java.lang.String, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String)
     *
     * @param name Player name.
     * @return New {@link Player}.
     */
    public Player createPlayer(String name) {
        if (mDatabase.isOpen()) {
            // Set values for database.
            ContentValues values = new ContentValues();
            values.put(DbContract.PlayerTable.COLUMN_NAME_PLAYER_NAME, name);
            values.put(DbContract.PlayerTable.COLUMN_NAME_PLAYER_SCORE, 1500);
            values.put(DbContract.PlayerTable.COLUMN_NAME_MODIFIED, (System.currentTimeMillis() / 1000L));

            // Insert values into database.
            long insertId = mDatabase.insert(DbContract.PlayerTable.TABLE_NAME, null, values);

            // Fetch player from database.
            Cursor cursor = mDatabase.query(
                    DbContract.PlayerTable.TABLE_NAME,
                    mColumnList,
                    DbContract.PlayerTable._ID + " = " + insertId,
                    null,
                    null,
                    null,
                    null);
            cursor.moveToFirst();

            Player player = cursorToPlayer(cursor);
            cursor.close();

            return player;
        } else {
            Log.e(TAG, "Trying to create player when source is not open, call open() before any operation.");
            return null;
        }
    }

    /**
     * Delete a player from database.
     * @param player {@link Player} to delete.
     * @return Whether operations was executed.
     */
    public boolean deletePlayer(Player player) {
        if (mDatabase.isOpen()) {
            int playerId = player.getId();
            mDatabase.delete(DbContract.PlayerTable.TABLE_NAME, DbContract.PlayerTable._ID + " = " + playerId, null);
            return true;
        } else {
            Log.e(TAG, "Trying to delete player when source is not open, call open() before any operation.");
            return false;
        }
    }

    /**
     * Fetch all players from database.
     * @return All players from database as {@link Player}.
     */
    public List<Player> getAllPlayers() {
        if (mDatabase.isOpen()) {
            List<Player> players = new ArrayList<>();

            // Fetch all players from database.
            Cursor cursor = mDatabase.query(DbContract.PlayerTable.TABLE_NAME, mColumnList, null, null, null, null, null);
            cursor.moveToFirst();

            // Go through all table rows.
            while (!cursor.isAfterLast()) {
                // Create a new player associated with the table row.
                Player player = cursorToPlayer(cursor);
                players.add(player);
                // Move to next table row.
                cursor.moveToNext();
            }
            cursor.close();

            return players;
        } else {
            Log.e(TAG, "Trying to fetch all players when source is not open, call open() before any operation.");
            return null;
        }
    }

    /**
     * Get player with id.
     * @param playerId Player id.
     * @return Player with id.
     */
    public Player getPlayer(int playerId) {
        if (mDatabase.isOpen()) {
            Cursor cursor = mDatabase.query(DbContract.PlayerTable.TABLE_NAME, mColumnList, DbContract.PlayerTable._ID + " = " + playerId, null, null, null, "1");
            cursor.moveToFirst();

            Player player = cursorToPlayer(cursor);

            cursor.close();

            return player;
        } else {
            return null;
        }
    }

    /**
     * Adjust player score based on players score alteration.
     * @param player Player to adjust score alteration on.
     * @return True if only one row was affected.
     */
    public boolean adjustPlayerScore(Player player) {
        if (mDatabase != null) {
            // Fields to update.
            ContentValues values = new ContentValues();
            values.put(DbContract.PlayerTable.COLUMN_NAME_PLAYER_SCORE, player.getScore() + (player.getScoreAlteration()));
            values.put(DbContract.PlayerTable.COLUMN_NAME_MODIFIED, (System.currentTimeMillis() / 1000L));

            // Update row.
            int rowsAffected = mDatabase.update(DbContract.PlayerTable.TABLE_NAME,
                    values,
                    DbContract.PlayerTable._ID + " = " + player.getId(),
                    null);
            return rowsAffected > 0;
        } else {
            Log.e(TAG, "Error while updating player");
            return false;
        }
    }

    /**
     * Create a {@link Player} from the provided cursor.
     *
     * @param cursor Cursor pointing to wanted player.
     * @return Player associated with the cursor pointer.
     */
    private Player cursorToPlayer(Cursor cursor) {
        int playerId = cursor.getInt(cursor.getColumnIndexOrThrow(DbContract.PlayerTable._ID));
        String playerName = cursor.getString(cursor.getColumnIndexOrThrow(DbContract.PlayerTable.COLUMN_NAME_PLAYER_NAME));
        int playerScore = cursor.getInt(cursor.getColumnIndexOrThrow(DbContract.PlayerTable.COLUMN_NAME_PLAYER_SCORE));
        int modified = cursor.getInt(cursor.getColumnIndexOrThrow(DbContract.PlayerTable.COLUMN_NAME_MODIFIED));

        Player player = new Player();
        player.setId(playerId);
        player.setName(playerName);
        player.setScore(playerScore);
        player.setModified(modified);

        return player;
    }
}
