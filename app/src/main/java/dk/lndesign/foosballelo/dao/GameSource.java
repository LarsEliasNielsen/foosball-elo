package dk.lndesign.foosballelo.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dk.lndesign.foosballelo.db.DbContract;
import dk.lndesign.foosballelo.db.DbHelper;
import dk.lndesign.foosballelo.model.Game;

/**
 * Data access point for {@link dk.lndesign.foosballelo.model.Game}.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class GameSource {

    private static final String TAG = GameSource.class.getSimpleName();

    // Database fields.
    private SQLiteDatabase mDatabase;
    private DbHelper mDbHelper;
    private String[] mColumnList = {
            DbContract.GameTable._ID,
            DbContract.GameTable.COLUMN_NAME_WINNING_TEAM,
            DbContract.GameTable.COLUMN_NAME_TIMESTAMP
    };

    public GameSource(Context context) {
        mDbHelper = new DbHelper(context);
    }

    /**
     * Open database.
     * Must be opened before operations.
     * @throws SQLException
     */
    public void open() throws SQLException {
        mDatabase = mDbHelper.getWritableDatabase();
    }

    public void close() {
        mDbHelper.close();
    }

    /**
     * Create a new game and return the newly created game.
     *
     * @param winningTeam Constant for winning team (eg. red or blue).
     * @return new {@link Game}.
     */
    public Game createGame(String winningTeam) {
        if (mDatabase.isOpen()) {
            // Set values for database.
            ContentValues values = new ContentValues();
            values.put(DbContract.GameTable.COLUMN_NAME_WINNING_TEAM, winningTeam);
            values.put(DbContract.GameTable.COLUMN_NAME_TIMESTAMP, (System.currentTimeMillis() / 1000L));

            // Insert values into database.
            long insertId = mDatabase.insert(DbContract.GameTable.TABLE_NAME, null, values);

            // Fetch player from database.
            Cursor cursor = mDatabase.query(
                    DbContract.GameTable.TABLE_NAME,
                    mColumnList,
                    DbContract.GameTable._ID + " = " + insertId,
                    null,
                    null,
                    null,
                    null);
            cursor.moveToFirst();

            Game game = cursorToGame(cursor);
            cursor.close();

            return game;
        } else {
            Log.e(TAG, "Trying to create game when source is not open, call open() before any operation.");
            return null;
        }
    }

    /**
     * Delete a {@link Game}.
     *
     * @param game Game to delete.
     * @return Whether operation was executed.
     */
    public boolean deleteGame(Game game) {
        if (mDatabase.isOpen()) {
            int gameId = game.getId();
            mDatabase.delete(DbContract.GameTable.TABLE_NAME, DbContract.GameTable._ID + " = " + gameId, null);
            return true;
        } else {
            Log.e(TAG, "Trying to delete game when source is not open, call open() before any operation.");
            return false;
        }
    }

    /**
     * Fetch all stored games.
     * @return List of all {@link Game}.
     */
    public List<Game> getAllGames() {
        if (mDatabase.isOpen()) {
            List<Game> games = new ArrayList<>();

            // Fetch all games from database.
            Cursor cursor = mDatabase.query(DbContract.GameTable.TABLE_NAME, mColumnList, null, null, null, null, null);
            cursor.moveToFirst();

            // Go through all table rows.
            while (!cursor.isAfterLast()) {
                // Create a new player associated with the table row.
                Game game = cursorToGame(cursor);
                games.add(game);
                // Move to next table row.
                cursor.moveToNext();
            }
            cursor.close();

            return games;
        } else {
            Log.e(TAG, "Trying to fetch all games when source is not open, call open() before any operation.");
            return null;
        }
    }

    /**
     * Create a {@link Game} from provided cursor.
     * @param cursor Cursor pointing to game.
     * @return Game associated with the cursor pointer.
     */
    private Game cursorToGame(Cursor cursor) {
        int gameId = cursor.getInt(cursor.getColumnIndexOrThrow(DbContract.GameTable._ID));
        String winningTeam = cursor.getString(cursor.getColumnIndexOrThrow(DbContract.GameTable.COLUMN_NAME_WINNING_TEAM));
        int timestamp = cursor.getInt(cursor.getColumnIndexOrThrow(DbContract.GameTable.COLUMN_NAME_TIMESTAMP));

        Game game = new Game();
        game.setId(gameId);
        game.setWinningTeam(winningTeam);
        game.setTimestamp(timestamp);

        return game;
    }
}
