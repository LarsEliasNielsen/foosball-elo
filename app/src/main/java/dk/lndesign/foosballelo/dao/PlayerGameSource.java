package dk.lndesign.foosballelo.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dk.lndesign.foosballelo.db.DbContract;
import dk.lndesign.foosballelo.db.DbHelper;
import dk.lndesign.foosballelo.model.PlayerGame;

/**
 * Data access point for {@link dk.lndesign.foosballelo.model.PlayerGame}.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class PlayerGameSource {

    private static final String TAG = GameSource.class.getSimpleName();

    // Database fields.
    private SQLiteDatabase mDatabase;
    private DbHelper mDbHelper;
    private String[] mColumnList = {
            DbContract.PlayerGameTable._ID,
            DbContract.PlayerGameTable.COLUMN_NAME_PLAYER_ID,
            DbContract.PlayerGameTable.COLUMN_NAME_GAME_ID,
            DbContract.PlayerGameTable.COLUMN_NAME_TEAM,
            DbContract.PlayerGameTable.COLUMN_NAME_PLAYER_SCORE_ALTER
    };

    public PlayerGameSource(Context context) {
        mDbHelper = new DbHelper(context);
    }

    /**
     * Open database.
     * Must be opened before operations.
     * @throws SQLException
     */
    public void open() throws SQLException {
        mDatabase = mDbHelper.getWritableDatabase();
    }

    public void close() {
        mDbHelper.close();
    }

    /**
     * Create a new player game.
     * @param playerId Player id.
     * @param gameId Game id.
     * @param team Player team.
     * @param scoreAlteration Player score alteration, amount of points lost/gained.
     * @return New {@link PlayerGame}.
     */
    public PlayerGame createPlayerGame(int playerId, int gameId, String team, int scoreAlteration) {
        if (mDatabase.isOpen()) {
            // Set values for database.
            ContentValues values = new ContentValues();
            values.put(DbContract.PlayerGameTable.COLUMN_NAME_PLAYER_ID, playerId);
            values.put(DbContract.PlayerGameTable.COLUMN_NAME_GAME_ID, gameId);
            values.put(DbContract.PlayerGameTable.COLUMN_NAME_TEAM, team);
            values.put(DbContract.PlayerGameTable.COLUMN_NAME_PLAYER_SCORE_ALTER, scoreAlteration);

            // Insert values into database.
            long insertId = mDatabase.insert(DbContract.PlayerGameTable.TABLE_NAME, null, values);

            // Fetch player game from database.
            Cursor cursor = mDatabase.query(
                    DbContract.PlayerGameTable.TABLE_NAME,
                    mColumnList,
                    DbContract.PlayerGameTable._ID + " = " + insertId,
                    null,
                    null,
                    null,
                    null);
            cursor.moveToFirst();

            PlayerGame playerGame = cursorToPlayerGame(cursor);
            cursor.close();

            return playerGame;
        } else {
            Log.e(TAG, "Trying to create player game when source is not open, call open() before any operation.");
            return null;
        }
    }

    /**
     * Delete a player game.
     * @param playerGame Player game to delete.
     * @return Whether opration was executed.
     */
    public boolean deletePlayerGame(PlayerGame playerGame) {
        if (mDatabase.isOpen()) {
            int playerGameId = playerGame.getId();
            mDatabase.delete(DbContract.PlayerGameTable.TABLE_NAME, DbContract.PlayerGameTable._ID + " = " + playerGameId, null);
            return true;
        } else {
            Log.e(TAG, "Trying to delete player game when source is not open, call open() before any operation.");
            return false;
        }
    }

    /**
     * Fetch all player games.
     * @return Lsit of player games.
     */
    public List<PlayerGame> getAllPlayerGames() {
        if (mDatabase.isOpen()) {
            List<PlayerGame> playerGames = new ArrayList<>();

            // Fetch all games from database.
            Cursor cursor = mDatabase.query(DbContract.PlayerGameTable.TABLE_NAME, mColumnList, null, null, null, null, null);
            cursor.moveToFirst();

            // Go through all table rows.
            while (!cursor.isAfterLast()) {
                // Create a new player associated with the table row.
                PlayerGame playerGame = cursorToPlayerGame(cursor);
                playerGames.add(playerGame);
                // Move to next table row.
                cursor.moveToNext();
            }
            cursor.close();

            return playerGames;
        } else {
            Log.e(TAG, "Trying to fetch all player games when source is not open, call open() before any operation.");
            return null;
        }
    }

    /**
     * Create a {@link PlayerGameSource} from cursor.
     * @param cursor Cursor pointing to player game.
     * @return Game created from cursor.
     */
    private PlayerGame cursorToPlayerGame(Cursor cursor) {
        int playerGameId = cursor.getInt(cursor.getColumnIndexOrThrow(DbContract.PlayerGameTable._ID));
        int playerId = cursor.getInt(cursor.getColumnIndexOrThrow(DbContract.PlayerGameTable.COLUMN_NAME_PLAYER_ID));
        int gameId = cursor.getInt(cursor.getColumnIndexOrThrow(DbContract.PlayerGameTable.COLUMN_NAME_GAME_ID));
        String team = cursor.getString(cursor.getColumnIndexOrThrow(DbContract.PlayerGameTable.COLUMN_NAME_TEAM));
        int scoreAlteration = cursor.getInt(cursor.getColumnIndexOrThrow(DbContract.PlayerGameTable.COLUMN_NAME_PLAYER_SCORE_ALTER));

        PlayerGame playerGame = new PlayerGame();
        playerGame.setId(playerGameId);
        playerGame.setPlayerId(playerId);
        playerGame.setGameId(gameId);
        playerGame.setTeam(team);
        playerGame.setPlayerScoreAlteration(scoreAlteration);

        return playerGame;
    }

}
