package dk.lndesign.foosballelo.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Helper for managing database and tables.
 * {@link DbContract} holds all fields and queries.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class DbHelper extends SQLiteOpenHelper {

    public DbHelper(Context context) {
        super(context, DbContract.DATABASE_NAME, null, DbContract.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DbContract.PlayerTable.CREATE_TABLE);
        db.execSQL(DbContract.GameTable.CREATE_TABLE);
        db.execSQL(DbContract.PlayerGameTable.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Empty tables.
        db.execSQL(DbContract.PlayerTable.DELETE_TABLE);
        db.execSQL(DbContract.GameTable.DELETE_TABLE);
        db.execSQL(DbContract.PlayerGameTable.DELETE_TABLE);
        onCreate(db);
    }
}
