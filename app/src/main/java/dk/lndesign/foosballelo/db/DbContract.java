package dk.lndesign.foosballelo.db;

import android.provider.BaseColumns;

/**
 * SQLite database contract that holds all tables associated with the Elo scoring system:
 *   A table containing all players.
 *   A table containing all games.
 *   A table containing player/game relation.
 * {@link DbHelper} will utilize field within this contract.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public final class DbContract {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "FoosballElo.db";
    private static final String TEXT_TYPE = " TEXT";
    private static final String INT_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";

    public DbContract() {
    }

    public static abstract class PlayerTable implements BaseColumns {
        public static final String TABLE_NAME = "player";
        public static final String COLUMN_NAME_PLAYER_NAME = "player_name";
        public static final String COLUMN_NAME_PLAYER_SCORE = "player_score";
        public static final String COLUMN_NAME_MODIFIED = "modified";

        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + INT_TYPE + " PRIMARY KEY," +
                COLUMN_NAME_PLAYER_NAME + TEXT_TYPE + COMMA_SEP +
                COLUMN_NAME_PLAYER_SCORE + INT_TYPE + COMMA_SEP +
                COLUMN_NAME_MODIFIED + INT_TYPE + " DEFAULT CURRENT_TIMESTAMP" +
                ")";
        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public static abstract class GameTable implements BaseColumns {
        public static final String TABLE_NAME = "game";
        public static final String COLUMN_NAME_WINNING_TEAM = "winning_team";
        public static final String COLUMN_NAME_TIMESTAMP = "modified";

        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + INT_TYPE + " PRIMARY KEY," +
                COLUMN_NAME_WINNING_TEAM + TEXT_TYPE + COMMA_SEP +
                COLUMN_NAME_TIMESTAMP + INT_TYPE + " DEFAULT CURRENT_TIMESTAMP" +
                ")";
        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public static abstract class PlayerGameTable implements BaseColumns {
        public static final String TABLE_NAME = "player_game";
        public static final String COLUMN_NAME_PLAYER_ID = "player_id";
        public static final String COLUMN_NAME_GAME_ID = "game_id";
        public static final String COLUMN_NAME_TEAM = "team";
        public static final String COLUMN_NAME_PLAYER_SCORE_ALTER = "player_score_alter";

        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + INT_TYPE + " PRIMARY KEY," +
                COLUMN_NAME_PLAYER_ID + INT_TYPE + COMMA_SEP +
                COLUMN_NAME_GAME_ID + INT_TYPE + COMMA_SEP +
                COLUMN_NAME_TEAM + TEXT_TYPE + COMMA_SEP +
                COLUMN_NAME_PLAYER_SCORE_ALTER + INT_TYPE + COMMA_SEP +
                "FOREIGN KEY(" + COLUMN_NAME_PLAYER_ID + ") REFERENCES " + PlayerTable.TABLE_NAME + "(" + PlayerTable._ID + ")," +
                "FOREIGN KEY(" + COLUMN_NAME_GAME_ID + ") REFERENCES " + GameTable.TABLE_NAME + "(" + GameTable._ID + ")" +
                ")";
        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }
}
