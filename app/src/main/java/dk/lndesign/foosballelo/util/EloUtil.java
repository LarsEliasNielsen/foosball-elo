package dk.lndesign.foosballelo.util;

import android.support.annotation.CheckResult;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import dk.lndesign.foosballelo.model.Player;
import dk.lndesign.foosballelo.model.Team;

/**
 * Created by larn on 08/11/15.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class EloUtil {

    private static final String TAG = EloUtil.class.getSimpleName();
    private static boolean mDebug = false;

    public EloUtil() {
    }

    public EloUtil(boolean debug) {
        mDebug = debug;
    }

    /**
     * Set verbose debugging.
     * @param debug Debug flag.
     */
    public static void setDebug(boolean debug) {
        mDebug = debug;
    }

    /**
     * Calculate scores for each player.
     * Players should have position:
     *   Index 0: Red player 1
     *   Index 1: Red player 2
     *   Index 2: Blue player 1
     *   Index 3: Blue player 2
     * TODO: Find a better way to parse and return players.
     * @param players List of red and blue players, should only contain a total of 4 players.
     * @param winningTeam Winning team.
     * @return List of players with score alterations.
     */
    @Nullable
    @CheckResult
    public static List<Player> calculateGameScore(List<Player> players, String winningTeam) {
        // Player fields.
        Player redPlayer1 = players.get(0);
        Player redPlayer2 = players.get(1);
        Player bluePlayer1 = players.get(2);
        Player bluePlayer2 = players.get(3);

        EloUtil.calculateGameScore(redPlayer1, redPlayer2, bluePlayer1, bluePlayer2, winningTeam);

        /**
         * Return players with scores.
         */
        List<Player> playerWithScore = new ArrayList<>();
        playerWithScore.add(0, redPlayer1);
        playerWithScore.add(1, redPlayer2);
        playerWithScore.add(2, bluePlayer1);
        playerWithScore.add(3, bluePlayer2);

        return playerWithScore;
    }

    /**
     * Calculate scores for each player.
     * @param redPlayer1 Player 1 on red team.
     * @param redPlayer2 Player 2 on red team.
     * @param bluePlayer1 Player 1 on blue team.
     * @param bluePlayer2 Player 2 on blue team.
     * @param winningTeam Winning team.
     */
    public static void calculateGameScore(Player redPlayer1, Player redPlayer2,
                                          Player bluePlayer1, Player bluePlayer2,
                                          String winningTeam) {
        // Rating and score fields.
        double redRating, blueRating;
        double redRating1, redRating2, blueRating1, blueRating2;
        double redScore, blueScore;

        /**
         * Average player score for each team.
         * We use the average team score to calculate rating for each team.
         */
        int averageRedScore = (redPlayer1.getScore() + redPlayer2.getScore()) / 2;
        int averageBlueScore = (bluePlayer1.getScore() + bluePlayer2.getScore()) / 2;
        if (mDebug) {
            Log.d(TAG, String.format("Average scores; red: %4d, blue: %4d", averageRedScore, averageBlueScore));
        }

        /**
         * Team performance rating.
         * The rating is the same regardless of who wins the match.
         */
        redRating = calculatePerformanceRating(averageRedScore, averageBlueScore);
        blueRating = calculatePerformanceRating(averageBlueScore, averageRedScore);
        if (mDebug) {
            Log.d(TAG, String.format("Team performance rating: red: %.3f, blue: %.3f", redRating, blueRating));
        }

        switch (winningTeam) {
            case Team.RED:
                /**
                 * Red players performance rating on red victory.
                 */
                if (redPlayer1.getScore() >= redPlayer2.getScore()) {
                    redRating1 = calculatePerformanceRating(redPlayer1.getScore(), redPlayer2.getScore());
                    redRating2 = calculatePerformanceRating(redPlayer2.getScore(), redPlayer1.getScore());
                } else {
                    redRating1 = calculatePerformanceRating(redPlayer2.getScore(), redPlayer1.getScore());
                    redRating2 = calculatePerformanceRating(redPlayer1.getScore(), redPlayer2.getScore());
                }
                if (bluePlayer1.getId() >= bluePlayer2.getScore()) {
                    blueRating1 = calculatePerformanceRating(bluePlayer2.getScore(), bluePlayer1.getScore());
                    blueRating2 = calculatePerformanceRating(bluePlayer1.getScore(), bluePlayer2.getScore());
                } else {
                    blueRating1 = calculatePerformanceRating(bluePlayer1.getScore(), bluePlayer2.getScore());
                    blueRating2 = calculatePerformanceRating(bluePlayer2.getScore(), bluePlayer1.getScore());
                }

                /**
                 * Team score for red victory.
                 */
                redScore = 24 * (1 - redRating);
                blueScore = 24 * (0 - blueRating);
                break;
            case Team.BLUE:
                /**
                 * Players performance rating on blue victory.
                 */
                if (redPlayer1.getScore() >= redPlayer2.getScore()) {
                    redRating1 = calculatePerformanceRating(redPlayer2.getScore(), redPlayer1.getScore());
                    redRating2 = calculatePerformanceRating(redPlayer1.getScore(), redPlayer2.getScore());
                } else {
                    redRating1 = calculatePerformanceRating(redPlayer1.getScore(), redPlayer2.getScore());
                    redRating2 = calculatePerformanceRating(redPlayer2.getScore(), redPlayer1.getScore());
                }
                if (bluePlayer1.getId() >= bluePlayer2.getScore()) {
                    blueRating1 = calculatePerformanceRating(bluePlayer1.getScore(), bluePlayer2.getScore());
                    blueRating2 = calculatePerformanceRating(bluePlayer2.getScore(), bluePlayer1.getScore());
                } else {
                    blueRating1 = calculatePerformanceRating(bluePlayer2.getScore(), bluePlayer1.getScore());
                    blueRating2 = calculatePerformanceRating(bluePlayer1.getScore(), bluePlayer2.getScore());
                }

                /**
                 * Team score for blue victory.
                 */
                redScore = 24 * (0 - redRating);
                blueScore = 24 * (1 - blueRating);
                break;
            default:
                return;
        }

        if (mDebug) {
            Log.d(TAG, String.format("Red team performance rating: 1: %.3f, 2: %.3f", redRating1, redRating2));
            Log.d(TAG, String.format("Blue team performance rating: 1: %.3f, 2: %.3f", blueRating1, blueRating2));
            Log.d(TAG, String.format("Team scores: red: %.3f, blue: %.3f", redScore, blueScore));
        }

        /**
         * Player score.
         */
        int redScore1 = (int) Math.round(redScore * redRating1);
        int redScore2 = (int) Math.round(redScore * redRating2);
        int blueScore1 = (int) Math.round(blueScore * blueRating1);
        int blueScore2 = (int) Math.round(blueScore * blueRating2);
        if (mDebug) {
            Log.d(TAG, String.format("Red team scores: 1: %2d, 2: %2d", redScore1, redScore2));
            Log.d(TAG, String.format("Blue team scores: 1: %2d, 2: %2d", blueScore1, blueScore2));
        }

        /**
         * Add score alterations.
         */
        redPlayer1.setScoreAlteration(redScore1);
        redPlayer2.setScoreAlteration(redScore2);
        bluePlayer1.setScoreAlteration(blueScore1);
        bluePlayer2.setScoreAlteration(blueScore2);
    }

    /**
     * Calculate performance rating using the "algorithm of 400".
     * The hypothetical rating can be used as an indicator for the likelihood of winning, a rating
     * of 0.60 for one team will give the other team a rating of 0.40.
     * Performance rating can only be calculated using a set of two scores, but can be used on
     * both team (average) scores and player scores.
     * {@see https://en.wikipedia.org/wiki/Elo_rating_system}
     *
     * @param score1 Score for team/player 1.
     * @param score2 Score for team/player 2.
     * @return Performance rating for team/player 1.
     */
    private static double calculatePerformanceRating(int score1, int score2) {
        return 1 / (1 + Math.pow(10, (score2 - score1) / 400d));
    }
}
