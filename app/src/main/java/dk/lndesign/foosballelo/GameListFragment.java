package dk.lndesign.foosballelo;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dk.lndesign.foosballelo.adapter.GameRecyclerAdapter;
import dk.lndesign.foosballelo.dao.GameSource;
import dk.lndesign.foosballelo.dao.PlayerGameSource;
import dk.lndesign.foosballelo.dao.PlayerSource;
import dk.lndesign.foosballelo.decorator.DividerItemDecoration;
import dk.lndesign.foosballelo.fragment.GameEntityDialog;
import dk.lndesign.foosballelo.listener.DialogListener;
import dk.lndesign.foosballelo.listener.ItemListener;
import dk.lndesign.foosballelo.model.Game;
import dk.lndesign.foosballelo.model.Player;
import dk.lndesign.foosballelo.model.PlayerGame;

/**
 * Created by larn on 06/10/15.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class GameListFragment extends Fragment implements ItemListener, DialogListener {

    private static final String TAG = GameListFragment.class.getSimpleName();

    // Sources.
    private PlayerSource mPlayerSource;
    private GameSource mGameSource;
    private PlayerGameSource mPlayerGameSource;

    // Lists containing source data.
    private List<Player> mPlayers = new ArrayList<>();
    private List<Game> mGames = new ArrayList<>();
    private List<PlayerGame> mPlayerGames = new ArrayList<>();

    // Views.
    RecyclerView mRecyclerView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    View noContentWarning;

    public GameListFragment() {}

    public static GameListFragment newInstance() {
        return new GameListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_game_list, container, false);
        noContentWarning = view.findViewById(R.id.no_content_warning);

        updateSources(inflater.getContext());

        // Get game list.
        mRecyclerView = (RecyclerView) view.findViewById(R.id.game_list);

        // Set decorator.
        mRecyclerView.addItemDecoration(new DividerItemDecoration(inflater.getContext(), DividerItemDecoration.VERTICAL_LIST));

        // Set linear layout manager.
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        // Set animations.
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        // Set adapter.
        setGameListAdapter();

        // Setting swipe to refresh.
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_game_list);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Delaying refresh to show indicator.
                // TODO: Should remove this delay.
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        updateGameListView();
                    }
                }, 1000);
            }
        });
        // Adding colors to refresh spinner.
        mSwipeRefreshLayout.setColorSchemeColors(
                R.color.primary,
                R.color.primary_dark,
                R.color.accent);

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            // Attach click listener when fragment is visible.
            FloatingActionButton floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab);
            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openEntryEditor();
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mPlayerSource.open();
            mGameSource.open();
            mPlayerGameSource.open();
        } catch (SQLException e) {
            Log.d(TAG, "onResume: Failed to open source(s)", e);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mPlayerSource.close();
        mGameSource.close();
        mPlayerGameSource.close();
    }

    @Override
    public void itemClicked(int gameId) {
        Log.d(TAG, "Game clicked: #" + gameId);
    }

    @Override
    public void itemCreated() {
        Log.d(TAG, "Game list updated, refreshing ...");
        updateGameListView();
    }

    private void updateSources(Context context) {
        mPlayerSource = new PlayerSource(context);
        mGameSource = new GameSource(context);
        mPlayerGameSource = new PlayerGameSource(context);
        try {
            mPlayerSource.open();
            mGameSource.open();
            mPlayerGameSource.open();
            // Fetch all source data.
            mPlayers = mPlayerSource.getAllPlayers();
            mGames = mGameSource.getAllGames();
            mPlayerGames = mPlayerGameSource.getAllPlayerGames();

//            for (Player player : mPlayers) {
//                Log.i(TAG, "Player: " + player.toString());
//            }
//            for (Game game : mGames) {
//                Log.i(TAG, "Game: " + game.toString());
//            }
//            for (PlayerGame playerGame : mPlayerGames) {
//                Log.i(TAG, "Player game: " + playerGame.toString());
//            }
        } catch (SQLException e) {
            Log.d(TAG, "onCreateView: Failed to open source(s)", e);
        }
    }

    private void setGameListAdapter() {
        Log.d(TAG, "player: " + mPlayers.size());
        Log.d(TAG, "games: " + mGames.size());

        if (!mGames.isEmpty() && !mPlayers.isEmpty() && !mPlayerGames.isEmpty()) {
            GameRecyclerAdapter adapter = new GameRecyclerAdapter(mGames, mPlayers, mPlayerGames, this);
            mRecyclerView.setAdapter(adapter);

            // Make sure the warning is not shown when we have content.
            mRecyclerView.setVisibility(View.VISIBLE);
            noContentWarning.setVisibility(View.GONE);
            Log.i(TAG, "We have content !");
        } else {
            mRecyclerView.setVisibility(View.GONE);
            noContentWarning.setVisibility(View.VISIBLE);
            Log.i(TAG, "No content !");
        }
    }

    private void updateGameListView() {
        updateSources(getActivity());
        setGameListAdapter();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * http://android-developers.blogspot.dk/2012/05/using-dialogfragments.html
     * https://github.com/codepath/android_guides/wiki/Using-DialogFragment
     */
    private void openEntryEditor() {
        GameEntityDialog dialogFragment = GameEntityDialog.newInstance();
        dialogFragment.setListener(this);
        dialogFragment.show(getFragmentManager(), "dialog");
    }
}
