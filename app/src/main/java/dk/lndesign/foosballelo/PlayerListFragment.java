package dk.lndesign.foosballelo;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dk.lndesign.foosballelo.adapter.PlayerRecyclerAdapter;
import dk.lndesign.foosballelo.dao.PlayerSource;
import dk.lndesign.foosballelo.decorator.DividerItemDecoration;
import dk.lndesign.foosballelo.fragment.PlayerEntityDialog;
import dk.lndesign.foosballelo.listener.DialogListener;
import dk.lndesign.foosballelo.listener.ItemListener;
import dk.lndesign.foosballelo.model.Player;

/**
 * Player list fragment.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class PlayerListFragment extends Fragment implements ItemListener, DialogListener {

    private final String TAG = this.getClass().getSimpleName();

    // Source for players.
    private PlayerSource mPlayerSource;

    // List containing all players fetched from source.
    private List<Player> mPlayers = new ArrayList<>();

    // Views.
    RecyclerView mRecyclerView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    View noContentWarning;

    public PlayerListFragment() {}

    public static PlayerListFragment newInstance(String title) {
        return new PlayerListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_player_list, container, false);
        noContentWarning = view.findViewById(R.id.no_content_warning);

        updatePlayers(inflater.getContext());

        // Get player list.
        mRecyclerView = (RecyclerView) view.findViewById(R.id.player_list);

        // Decorator.
        mRecyclerView.addItemDecoration(new DividerItemDecoration(inflater.getContext(), DividerItemDecoration.VERTICAL_LIST));

        // Set linear layout manager.
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        // Set animations.
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        // Create and set adapter.
        setPlayerListAdapter();

        // Setting swipe to refresh.
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_player_list);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Delaying refresh to show indicator.
                // TODO: Should remove this delay.
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        updatePlayerListView();
                    }
                }, 1000);
            }
        });
        // Adding colors to refresh spinner.
        mSwipeRefreshLayout.setColorSchemeColors(
                R.color.primary,
                R.color.primary_dark,
                R.color.accent);

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            // Attach click listener on fab when fragment is visible.
            FloatingActionButton floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab);
            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openEntryEditor();
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mPlayerSource.open();
        } catch (SQLException e) {
            Log.d(TAG, "onResume: Failed to open source", e);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mPlayerSource.close();
    }

    @Override
    public void itemClicked(int playerId) {
        Log.d(TAG, "Player clicked: #" + playerId);
    }

    @Override
    public void itemCreated() {
        Log.d(TAG, "Player list updated, refreshing ...");
        updatePlayerListView();
    }

    /**
     * Callback for updating player list view.
     * TODO: Can we use a listener instead?
     */
    public void updatePlayerListView() {
        updatePlayers(getActivity());
        setPlayerListAdapter();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * Setting adapter to list view.
     * Call updatePlayers() before this.
     */
    private void setPlayerListAdapter() {
        if (!mPlayers.isEmpty()) {
            PlayerRecyclerAdapter adapter = new PlayerRecyclerAdapter(mPlayers, this);
            mRecyclerView.setAdapter(adapter);

            mRecyclerView.setVisibility(View.VISIBLE);
            noContentWarning.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            noContentWarning.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Fetching players and setting result to member field.
     * Player source is a member field, so we can open/close it in fragment lifecycle.
     *
     * @param context Activity context.
     */
    private void updatePlayers(Context context) {
        mPlayerSource = new PlayerSource(context);
        try {
            mPlayerSource.open();
            // Fetch all players.
            mPlayers = mPlayerSource.getAllPlayers();
        } catch (SQLException e) {
            Log.d(TAG, "onCreateView: Failed to open source", e);
        }
    }

    /**
     * http://android-developers.blogspot.dk/2012/05/using-dialogfragments.html
     * https://github.com/codepath/android_guides/wiki/Using-DialogFragment
     */
    private void openEntryEditor() {
        PlayerEntityDialog dialogFragment = PlayerEntityDialog.newInstance();
        dialogFragment.setListener(this);
        dialogFragment.show(getFragmentManager(), "dialog");
    }
}
