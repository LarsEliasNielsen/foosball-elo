package dk.lndesign.foosballelo.model;

/**
 * Created by larn on 15/09/15.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class PlayerGame {

    private final String TAG = this.getClass().getSimpleName();

    private int id;
    private int playerId;
    private int gameId;
    private String team;
    private int playerScoreAlteration;

    public PlayerGame() {}

    public PlayerGame(int playerId, int gameId, String team, int playerScoreAlteration) {
        this.playerId = playerId;
        this.gameId = gameId;
        this.team = team;
        this.playerScoreAlteration = playerScoreAlteration;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public int getPlayerScoreAlteration() {
        return playerScoreAlteration;
    }

    public void setPlayerScoreAlteration(int scoreAlteration) {
        this.playerScoreAlteration = scoreAlteration;
    }

    public String toString() {
        return String.format("PlayerGame { id:%s, playerId:%s, gameId:%s, team:%s, playerScoreAlteration:%d }",
                id, playerId, gameId, team, playerScoreAlteration);
    }
}
