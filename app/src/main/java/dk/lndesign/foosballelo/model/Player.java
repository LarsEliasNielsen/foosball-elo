package dk.lndesign.foosballelo.model;

/**
 * Created by larn on 15/09/15.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class Player {

    private final String TAG = this.getClass().getSimpleName();

    private int id;
    private String name;
    private int score;
    // Team and score alteration are only used when fused with game relation.
    private String team;
    private int scoreAlteration;
    private int modified;

    public Player() {}

    public Player(String name) {
        this.name = name;
        this.score = 1500;
        this.modified = (int) (System.currentTimeMillis() / 1000L);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public int getScoreAlteration() {
        return scoreAlteration;
    }

    public void setScoreAlteration(int scoreAlteration) {
        this.scoreAlteration = scoreAlteration;
    }

    public int getModified() {
        return modified;
    }

    public void setModified(int modified) {
        this.modified = modified;
    }

    public String toString() {
        return String.format("Player { id: %s, name: %s, score: %d, team: %s, scoreAlteration: %d }",
                id, name, score, team, scoreAlteration);
    }
}
