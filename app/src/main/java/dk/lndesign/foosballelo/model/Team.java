package dk.lndesign.foosballelo.model;

/**
 * Created by larn on 15/09/15.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class Team {
    public static final String RED = "red";
    public static final String BLUE = "blue";
}
