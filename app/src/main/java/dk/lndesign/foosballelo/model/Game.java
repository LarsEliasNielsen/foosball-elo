package dk.lndesign.foosballelo.model;

/**
 * Created by larn on 15/09/15.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class Game {

    private final String TAG = this.getClass().getSimpleName();

    private int id;
    private String winningTeam;
    private long timestamp;

    public Game() {}

    public Game(String winningTeam) {
        this.winningTeam = winningTeam;
        this.timestamp = System.currentTimeMillis() / 1000L;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWinningTeam() {
        return winningTeam;
    }

    public void setWinningTeam(String winningTeam) {
        this.winningTeam = winningTeam;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public String toString() {
        return String.format("GameTable { id:%s, winningTeam:%s, timestamp:%s }", id, winningTeam, timestamp);
    }
}
