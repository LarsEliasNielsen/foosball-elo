package dk.lndesign.foosballelo.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import dk.lndesign.foosballelo.R;

/**
 * Created by larn on 16/09/15.
 *
 * @author Lars Nielsen <larn@tv2.dk>.
 */
public class PlayerCursorAdapter extends ResourceCursorAdapter {

    public PlayerCursorAdapter(Context context, int layout, Cursor cursor, int flags) {
        super(context, layout, cursor, flags);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
//        TextView rowEntityId = (TextView) view.findViewById(R.id.listview_row_entry_id);
//        TextView rowTitle = (TextView) view.findViewById(R.id.listview_row_title);
//        TextView rowSubtitle = (TextView) view.findViewById(R.id.listview_row_subtitle);
//        TextView rowModified = (TextView) view.findViewById(R.id.listview_row_modified);
        TextView redPlayer1 = (TextView) view.findViewById(R.id.textview_red_player_1);
        TextView redPlayer2 = (TextView) view.findViewById(R.id.textview_red_player_2);
        TextView bluePlayer1 = (TextView) view.findViewById(R.id.textview_blue_player_1);
        TextView bluePlayer2 = (TextView) view.findViewById(R.id.textview_blue_player_2);

//        // Write both row id and entity id.
//        rowEntityId.setText(
//                "[" + cursor.getString(cursor.getColumnIndexOrThrow(MyContract.MyEntry._ID)) + "] " +
//                        cursor.getString(cursor.getColumnIndexOrThrow(MyContract.MyEntry.COLUMN_NAME_ENTRY_ID))
//        );
//        rowTitle.setText(cursor.getString(cursor.getColumnIndexOrThrow(MyContract.MyEntry.COLUMN_NAME_TITLE)));
//        rowSubtitle.setText(cursor.getString(cursor.getColumnIndexOrThrow(MyContract.MyEntry.COLUMN_NAME_SUBTITLE)));

//        // We get unix timestamp and format it to readable data and time.
////        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm Z", Locale.getDefault());
//        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm", context.getResources().getConfiguration().locale);
//        int modified = cursor.getInt(cursor.getColumnIndexOrThrow(MyContract.MyEntry.COLUMN_NAME_MODIFIED));
//        Date modifiedDate = new Date(modified * 1000L);
//        rowModified.setText(sdf.format(modifiedDate));



//        // Set view tag to primary key from database.
//        view.getTag(cursor.getColumnIndexOrThrow(MyContract.MyEntry._ID));
    }
}
