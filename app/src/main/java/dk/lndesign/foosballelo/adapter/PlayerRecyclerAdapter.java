package dk.lndesign.foosballelo.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import dk.lndesign.foosballelo.R;
import dk.lndesign.foosballelo.listener.ItemListener;
import dk.lndesign.foosballelo.model.Player;

/**
 * Created by larn on 16/09/15.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class PlayerRecyclerAdapter extends RecyclerView.Adapter<PlayerRecyclerAdapter.PlayerViewHolder> implements View.OnClickListener {

    /**
     * View holder for player.
     */
    public static class PlayerViewHolder extends RecyclerView.ViewHolder {
        public View itemLayout;
        public TextView mPlayerName;
        public TextView mPlayerScore;
        public TextView mModified;

        public PlayerViewHolder(View layout) {
            super(layout);
            this.itemLayout = layout;
            this.mPlayerName = (TextView) layout.findViewById(R.id.textview_player_name);
            this.mPlayerScore = (TextView) layout.findViewById(R.id.textview_player_score);
            this.mModified = (TextView) layout.findViewById(R.id.textview_modified);
        }
    }

    private List<Player> mPlayers = new ArrayList<>();
    private ItemListener mListener;

    public PlayerRecyclerAdapter(List<Player> players, ItemListener listener) {
        this.mPlayers = players;
        this.mListener = listener;
    }

    @Override
    public PlayerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.player_listview_row, parent, false);

        PlayerViewHolder playerViewHolder = new PlayerViewHolder(view);
        playerViewHolder.itemLayout.setOnClickListener(this);

        return playerViewHolder;
    }

    @Override
    public void onBindViewHolder(PlayerViewHolder holder, int position) {
        Player player = mPlayers.get(position);

        holder.mPlayerName.setText(player.getName());
        holder.mPlayerScore.setText(String.format("%d", player.getScore()));
        holder.mModified.setText(getReadableTimestamp(player.getModified()));

        // Set player id as tag.
        holder.itemLayout.setTag(player.getId());
    }

    @Override
    public int getItemCount() {
        return mPlayers.size();
    }

    @Override
    public void onClick(View view) {
        mListener.itemClicked(Integer.parseInt(view.getTag().toString()));
    }

    /**
     * Convert a Unix timestamp to a readable date and time.
     *
     * @param timestamp Unix timestamp.
     * @return Readable date and time.
     */
    private String getReadableTimestamp(int timestamp) {
        Date date = new Date(timestamp * 1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault());

        return sdf.format(date);
    }
}
