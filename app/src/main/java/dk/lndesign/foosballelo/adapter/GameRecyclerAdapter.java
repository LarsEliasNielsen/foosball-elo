package dk.lndesign.foosballelo.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import dk.lndesign.foosballelo.R;
import dk.lndesign.foosballelo.listener.ItemListener;
import dk.lndesign.foosballelo.model.Game;
import dk.lndesign.foosballelo.model.Player;
import dk.lndesign.foosballelo.model.PlayerGame;
import dk.lndesign.foosballelo.model.Team;

/**
 * Created by larn on 07/10/15.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class GameRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    /**
     * View holder for games.
     */
    private static class GameViewHolder extends RecyclerView.ViewHolder {
        View itemLayout;
        TextView gameStateRed;
        TextView gameStateBlue;
        TextView playerRed1;
        TextView playerRed2;
        TextView playerBlue1;
        TextView playerBlue2;

        public GameViewHolder(View layout) {
            super(layout);
            itemLayout = layout;
            gameStateRed = (TextView) layout.findViewById(R.id.game_state_red);
            gameStateBlue = (TextView) layout.findViewById(R.id.game_state_blue);
            playerRed1 = (TextView) layout.findViewById(R.id.textview_red_player_1);
            playerRed2 = (TextView) layout.findViewById(R.id.textview_red_player_2);
            playerBlue1 = (TextView) layout.findViewById(R.id.textview_blue_player_1);
            playerBlue2 = (TextView) layout.findViewById(R.id.textview_blue_player_2);
        }
    }

    private static final String TAG = GameRecyclerAdapter.class.getSimpleName();

    private List<Player> mPlayers;
    private List<Game> mGames;
    private List<PlayerGame> mPlayerGames;
    private ItemListener mListener;

    public GameRecyclerAdapter(List<Game> games, List<Player> players, List<PlayerGame> playerGames, ItemListener listener) {
        mPlayers = players;
        mGames = games;
        mPlayerGames = playerGames;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = inflater.inflate(R.layout.game_listview_row, parent, false);
        GameViewHolder gameViewHolder = new GameViewHolder(view);
        // Set click listener.
        gameViewHolder.itemLayout.setOnClickListener(this);

        return gameViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        // Get game from all games.
        Game game = mGames.get(position);
        // Find players via player game relation.
        List<Player> players = findPlayersForGame(game);
        // Find
        List<Player> redTeam = getTeamPlayers(players, Team.RED);
        List<Player> blueTeam = getTeamPlayers(players, Team.BLUE);
        // View.
        GameViewHolder gameViewHolder = (GameViewHolder) holder;

        gameViewHolder.gameStateRed.setText(game.getWinningTeam().equals(Team.RED) ? "WINNER" : "LOSER");
        gameViewHolder.gameStateBlue.setText(game.getWinningTeam().equals(Team.RED) ? "LOSER" : "WINNER");
        gameViewHolder.playerRed1.setText(String.format("%s (%d)", redTeam.get(0).getName(), redTeam.get(0).getScoreAlteration()));
        gameViewHolder.playerRed2.setText(String.format("%s (%d)", redTeam.get(1).getName(), redTeam.get(1).getScoreAlteration()));
        gameViewHolder.playerBlue1.setText(String.format("%s (%d)", blueTeam.get(0).getName(), blueTeam.get(0).getScoreAlteration()));
        gameViewHolder.playerBlue2.setText(String.format("%s (%d)", blueTeam.get(1).getName(), blueTeam.get(1).getScoreAlteration()));

        // Set game id as tag, used when item is clicked.
        // TODO: Is this the best way to parse game id?
        gameViewHolder.itemLayout.setTag(game.getId());
    }

    @Override
    public int getItemCount() {
        return mGames.size();
    }

    @Override
    public void onClick(View view) {
        mListener.itemClicked(Integer.parseInt(view.getTag().toString()));
    }

    /**
     * Find players for a specific game.
     * The player game relation is used to look up players in game.
     *
     * @param game Game to find players for.
     * @return List fo players from game.
     */
    private List<Player> findPlayersForGame(Game game) {
        List<Player> foundPlayers = new ArrayList<>();

        for (PlayerGame playerGame : mPlayerGames) {
            if (game.getId() == playerGame.getGameId()) {
                for (Player player : mPlayers) {
                    if (player.getId() == playerGame.getPlayerId()) {
                        // Fuse PlayerGame data into Player.
                        player.setTeam(playerGame.getTeam());
                        player.setScoreAlteration(playerGame.getPlayerScoreAlteration());
                        foundPlayers.add(player);
                    }
                }
            }
        }

        return foundPlayers;
    }

    /**
     * Find team players in a list of players from game.
     * Players must have field team set.
     *
     * @param players Players from a single game.
     * @param team Team to get players for.
     * @return A list of players from specified team.
     */
    private List<Player> getTeamPlayers(List<Player> players, String team) {
        List<Player> teamPlayers = new ArrayList<>();

        for (Player player : players) {
            if (player.getTeam() != null && player.getTeam().equalsIgnoreCase(team)) {
                teamPlayers.add(player);
            }
        }

        return teamPlayers;
    }
}
