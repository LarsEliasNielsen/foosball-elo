package dk.lndesign.foosballelo.adapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import dk.lndesign.foosballelo.GameListFragment;
import dk.lndesign.foosballelo.PlayerListFragment;

/**
 * Pager adapter for view pager on main activity.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class MainPagerAdapter extends FragmentPagerAdapter {

    public static final int DEFAULT_TAB_INDEX = 0;

    // This MUST hold all fragment titles.
    private static String[] mFragmentTitles = {
            "Games",
            "Players",
            "Players"
    };

    public MainPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return GameListFragment.newInstance();
            case 1:
                return PlayerListFragment.newInstance(mFragmentTitles[position]);
            case 2:
                return PlayerListFragment.newInstance(mFragmentTitles[position]);
            default:
                return PlayerListFragment.newInstance("Wrong way");
        }
    }

    @Override
    public int getCount() {
        return mFragmentTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitles[position];
    }
}
