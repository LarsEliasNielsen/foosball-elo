package dk.lndesign.foosballelo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import dk.lndesign.foosballelo.R;
import dk.lndesign.foosballelo.model.Player;

/**
 * Created by larn on 05/10/15.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class PlayerSpinnerAdapter extends ArrayAdapter<Player> {

    private Context mContext;
    private List<Player> mPlayers;

    public PlayerSpinnerAdapter(Context context, int resource, List<Player> players) {
        super(context, resource, players);
        mContext = context;
        mPlayers = players;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    private View getCustomView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            row = inflater.inflate(R.layout.player_spinner_row, parent, false);
        }

        TextView playerName = (TextView) row.findViewById(R.id.player_name);
        TextView playerScore = (TextView) row.findViewById(R.id.player_score);

        playerName.setText(mPlayers.get(position).getName());
        playerScore.setText(String.format("%d", mPlayers.get(position).getScore()));

        return row;
    }

    @Override
    public int getCount() {
        return mPlayers.size();
    }
}
