package dk.lndesign.foosballelo;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.Toolbar;

import dk.lndesign.foosballelo.adapter.MainPagerAdapter;

/**
 * Main activity.
 * Holds a view pager with tabs to navigate.
 *
 * @author Lars Nielsen <lars@lndesign.dk>
 */
public class MainActivity extends FragmentActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String TAB_POSITION = "tab_position";
    private ViewPager mViewPager;
    private TabLayout mTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Toolbar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setActionBar(toolbar);

        // Global fab.
        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        // Initial fab animation.
        animateFab(fab, MainPagerAdapter.DEFAULT_TAB_INDEX);

        // View pager.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        // Adapter
        MainPagerAdapter pagerAdapter = new MainPagerAdapter(getFragmentManager());
        mViewPager.setAdapter(pagerAdapter);
        // Select center fragment in view pager.
        mViewPager.setCurrentItem(MainPagerAdapter.DEFAULT_TAB_INDEX);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                animateFab(fab, position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });

        // Tabs
        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(TAB_POSITION, mTabLayout.getSelectedTabPosition());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Set current tab or default.
        mViewPager.setCurrentItem(savedInstanceState.getInt(TAB_POSITION, MainPagerAdapter.DEFAULT_TAB_INDEX));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Animate fab and set correct resources (eg. icon).
     * @param fab Float action button.
     * @param position Fragment position, matching resources.
     */
    protected void animateFab(final FloatingActionButton fab, final int position) {
        final int[] iconArray = {
                R.drawable.ic_add_circle_outline_white_24dp,
                R.drawable.ic_person_add_white_24dp,
                R.drawable.ic_add_white_24dp
        };

        fab.clearAnimation();
        // Scale down animation
        ScaleAnimation shrink =  new ScaleAnimation(1f, 0.2f, 1f, 0.2f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        shrink.setDuration(150);
        shrink.setInterpolator(new DecelerateInterpolator());
        shrink.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                // Change fab icon.
                fab.setImageDrawable(getResources().getDrawable(iconArray[position], null));

                // Scale up animation
                ScaleAnimation expand = new ScaleAnimation(0.2f, 1f, 0.2f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                expand.setDuration(100);
                expand.setInterpolator(new AccelerateInterpolator());
                fab.startAnimation(expand);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        fab.startAnimation(shrink);
    }
}
