package dk.lndesign.foosballelo;

import android.test.AndroidTestCase;

import junit.framework.Assert;

import java.util.List;

import dk.lndesign.foosballelo.dao.GameSource;
import dk.lndesign.foosballelo.model.Game;
import dk.lndesign.foosballelo.model.Team;

/**
 * Created by larn on 30/09/15.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class GameSourceTest extends AndroidTestCase {

    private GameSource mGameSource;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mGameSource = new GameSource(getContext());
        mGameSource.open();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        mGameSource.close();
    }

    /**
     * Test that we cannot perform operations when source is closed.
     */
    public void testClosedSource() {
        // Close source that was opened in setUp.
        mGameSource.close();

        // Test createGame() when source is closed.
        Game game1 = mGameSource.createGame(Team.BLUE);
        Assert.assertNull(game1);

        // Test deleteGame() when source is closed.
        Game game2 = new Game(Team.BLUE);
        boolean success = mGameSource.deleteGame(game2);
        Assert.assertFalse(success);

        // Test getAllGames() when source is closed.
        List<Game> games = mGameSource.getAllGames();
        Assert.assertNull(games);
    }

    /**
     * Test that we can create a new game and it's returned with valid fields.
     * We also test that the game is present when fetching all games from source.
     */
    public void testCreateAndFetchGame() {
        // Create new game.
        Game game = mGameSource.createGame(Team.RED);
        // Save game id.
        int gameId = game.getId();

        // Check fields on object.
        Assert.assertNotNull(game);
        Assert.assertNotNull(game.getId());
        Assert.assertNotNull(game.getWinningTeam());
        Assert.assertNotNull(game.getTimestamp());

        // Get all games to look for game.
        List<Game> games = mGameSource.getAllGames();
        boolean objectFound = false;
        for (Game databaseGame : games) {
            if (databaseGame.getId() == gameId)
                objectFound = true;
        }
        Assert.assertTrue(objectFound);

        // Cleanup.
        mGameSource.deleteGame(game);
    }

    /**
     * Test that we can create a game and delete it.
     * We also test that the game is removed from database.
     */
    public void testDeleteGame() {
        // Create new game and save it.
        Game game = mGameSource.createGame(Team.RED);
        int gameId = game.getId();

        // Delete game.
        mGameSource.deleteGame(game);

        // Get all games to look for deleted game.
        List<Game> games = mGameSource.getAllGames();
        for (Game databaseGame : games) {
            Assert.assertNotSame("Same game found after deletion", databaseGame.getId(), gameId);
        }
    }
}
