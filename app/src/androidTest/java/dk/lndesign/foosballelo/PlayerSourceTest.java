package dk.lndesign.foosballelo;

import android.test.AndroidTestCase;

import junit.framework.Assert;

import java.util.List;

import dk.lndesign.foosballelo.dao.PlayerSource;
import dk.lndesign.foosballelo.model.Player;

/**
 * Created by larn on 30/09/15.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class PlayerSourceTest extends AndroidTestCase {

    private PlayerSource mPlayerSource;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mPlayerSource = new PlayerSource(getContext());
        mPlayerSource.open();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        mPlayerSource.close();
    }

    /**
     * Test that we cannot perform operations when source is closed.
     */
    public void testClosedSource() {
        // Close source that was opened in setUp.
        mPlayerSource.close();

        // Test createPlayer() when source is closed.
        Player player1 = mPlayerSource.createPlayer("testPlayer1");
        Assert.assertNull(player1);

        // Test deleteGame() when source is closed.
        Player player2 = new Player("testPlayer2");
        boolean success = mPlayerSource.deletePlayer(player2);
        Assert.assertFalse(success);

        // Test getAllGames() when source is closed.
        List<Player> players = mPlayerSource.getAllPlayers();
        Assert.assertNull(players);
    }

    /**
     * Test that we can create a new player and it's returned with valid fields.
     * We also test that the player is present when fetching all players from source.
     */
    public void testCreateAndFetchPlayer() {
        // Create new player.
        Player player = mPlayerSource.createPlayer("testPlayer3");
        // Save player id.
        int playerId = player.getId();

        // Check fields on object.
        Assert.assertNotNull(player);
        Assert.assertNotNull(player.getId());
        Assert.assertNotNull(player.getName());
        Assert.assertNotNull(player.getScore());
        Assert.assertNotNull(player.getModified());

        // Get all players to look for newly created player.
        List<Player> players = mPlayerSource.getAllPlayers();
        boolean objectFound = false;
        for (Player databasePlayer : players) {
            if (databasePlayer.getId() == playerId)
                objectFound = true;
        }
        Assert.assertTrue(objectFound);

        // Cleanup.
        mPlayerSource.deletePlayer(player);
    }

    /**
     * Test that we can create a player and delete it.
     * We also test that the player is removed from database.
     */
    public void testDeletePlayer() {
        // Create new player and save it.
        Player player = mPlayerSource.createPlayer("testPlayer4");
        int playerId = player.getId();

        // Delete player.
        mPlayerSource.deletePlayer(player);

        // Get all players to look for recently deleted player.
        List<Player> players = mPlayerSource.getAllPlayers();
        for (Player databasePlayer : players) {
            Assert.assertNotSame("Same player found after deletion", databasePlayer.getId(), playerId);
        }
    }
}
