package dk.lndesign.foosballelo;

import android.test.AndroidTestCase;

import junit.framework.Assert;

import java.util.List;

import dk.lndesign.foosballelo.dao.PlayerGameSource;
import dk.lndesign.foosballelo.model.PlayerGame;
import dk.lndesign.foosballelo.model.Team;

/**
 * Created by larn on 07/12/15.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class PlayerGameSourceTest extends AndroidTestCase {

    private PlayerGameSource mPlayerGameSource;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mPlayerGameSource = new PlayerGameSource(getContext());
        mPlayerGameSource.open();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        mPlayerGameSource.close();
    }

    /**
     * Test that we cannot perform operations when source is closed.
     */
    public void testClosedSource() {
        // Close source that was opened in setUp.
        mPlayerGameSource.close();

        // Test createPlayerGame() when source is closed.
        PlayerGame playerGame1 = mPlayerGameSource.createPlayerGame(1, 1, Team.RED, 0);
        Assert.assertNull(playerGame1);

        // Test deletePlayerGame() when source is closed.
        PlayerGame playerGame2 = new PlayerGame(2, 2, Team.BLUE, 0);
        boolean success = mPlayerGameSource.deletePlayerGame(playerGame2);
        Assert.assertFalse(success);

        // Test getAllPlayerGames() when source is closed.
        List<PlayerGame> playerGames = mPlayerGameSource.getAllPlayerGames();
        Assert.assertNull(playerGames);
    }

    /**
     * Test that we can create a new player game and it's returned with valid fields.
     * We also test that the player game is present when fetching all player games from source.
     */
    public void testCreateAndFetchGame() {
        // Create new player game.
        PlayerGame playerGame = mPlayerGameSource.createPlayerGame(1, 1, Team.RED, 0);
        // Save player game id.
        int playerGameId = playerGame.getId();

        // Check fields on object.
        Assert.assertNotNull(playerGame);
        Assert.assertNotNull(playerGame.getId());
        Assert.assertNotNull(playerGame.getPlayerId());
        Assert.assertNotNull(playerGame.getGameId());
        Assert.assertNotNull(playerGame.getTeam());
        Assert.assertNotNull(playerGame.getPlayerScoreAlteration());

        // Get all player games to look for player game.
        List<PlayerGame> playerGames = mPlayerGameSource.getAllPlayerGames();
        boolean objectFound = false;
        for (PlayerGame databasePlayerGame : playerGames) {
            if (databasePlayerGame.getId() == playerGameId)
                objectFound = true;
        }
        Assert.assertTrue(objectFound);

        // Cleanup.
        mPlayerGameSource.deletePlayerGame(playerGame);
    }

    /**
     * Test that we can create a player game and delete it.
     * We also test that the player game is removed from database.
     */
    public void testDeletePlayerGame() {
        // Create new player game and save it.
        PlayerGame playerGame = mPlayerGameSource.createPlayerGame(1, 1, Team.RED, 0);
        int playerGameId = playerGame.getId();

        // Delete player game.
        mPlayerGameSource.deletePlayerGame(playerGame);

        // Get all player games to look for deleted  player game.
        List<PlayerGame> playerGames = mPlayerGameSource.getAllPlayerGames();
        for (PlayerGame databasePlayerGame : playerGames) {
            Assert.assertNotSame("Same game found after deletion", databasePlayerGame.getId(), playerGameId);
        }
    }
}
