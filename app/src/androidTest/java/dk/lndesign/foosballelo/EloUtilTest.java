package dk.lndesign.foosballelo;

import android.test.AndroidTestCase;

import junit.framework.Assert;

import java.util.ArrayList;
import java.util.List;

import dk.lndesign.foosballelo.model.Player;
import dk.lndesign.foosballelo.model.Team;
import dk.lndesign.foosballelo.util.EloUtil;

/**
 * Created by larn on 09/11/15.
 *
 * @author Lars Nielsen <lars@lndesign.dk>.
 */
public class EloUtilTest extends AndroidTestCase {
    Player redPlayer1, redPlayer2, bluePlayer1, bluePlayer2;
    List<Player> players = new ArrayList<>();

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        // Create new players.
        redPlayer1 = new Player("Red One");
        redPlayer2 = new Player("Red Two");
        bluePlayer1 = new Player("Blue One");
        bluePlayer2 = new Player("Blue Two");

        // Add to list.
        players.add(0, redPlayer1);
        players.add(1, redPlayer2);
        players.add(2, bluePlayer1);
        players.add(3, bluePlayer2);

        EloUtil.setDebug(true);
    }

    /**
     * Test calculations when all players have the same score to start with.
     */
    public void testGameScoreOnEqualPlayerScores() {
        // Calculate game scores.
        players = EloUtil.calculateGameScore(players, Team.RED);

        // Check that we got a result.
        Assert.assertNotNull(players);

        // Check all player scores.
        Assert.assertEquals(6, players.get(0).getScoreAlteration());
        Assert.assertEquals(6, players.get(1).getScoreAlteration());
        Assert.assertEquals(-6, players.get(2).getScoreAlteration());
        Assert.assertEquals(-6, players.get(3).getScoreAlteration());

        // Check that absolute scores are "equal" on both sides, so we don't loose/gain points.
        Assert.assertEquals(
                Math.abs(players.get(0).getScoreAlteration() + players.get(1).getScoreAlteration()),
                Math.abs(players.get(2).getScoreAlteration() + players.get(3).getScoreAlteration()));
    }

    /**
     * Test calculations when the highly skilled team wins.
     */
    public void testGameScoresOnHighWin() {
        // Edit player scores.
        players.get(0).setScore(1650);
        players.get(1).setScore(1690);
        players.get(2).setScore(1320);
        players.get(3).setScore(1350);

        players = EloUtil.calculateGameScore(players, Team.RED);

        Assert.assertNotNull(players);

        Assert.assertEquals(2, players.get(0).getScoreAlteration());
        Assert.assertEquals(1, players.get(1).getScoreAlteration());
        Assert.assertEquals(-1, players.get(2).getScoreAlteration());
        Assert.assertEquals(-2, players.get(3).getScoreAlteration());

        Assert.assertEquals(
                Math.abs(players.get(0).getScoreAlteration() + players.get(1).getScoreAlteration()),
                Math.abs(players.get(2).getScoreAlteration() + players.get(3).getScoreAlteration()));
    }

    /**
     * Test calculations when the highly skilled team loses.
     */
    public void testGameScoresOnLowWin() {
        players.get(0).setScore(1650);
        players.get(1).setScore(1690);
        players.get(2).setScore(1320);
        players.get(3).setScore(1350);

        players = EloUtil.calculateGameScore(players, Team.BLUE);

        Assert.assertNotNull(players);

        Assert.assertEquals(-9, players.get(0).getScoreAlteration());
        Assert.assertEquals(-12, players.get(1).getScoreAlteration());
        Assert.assertEquals(11, players.get(2).getScoreAlteration());
        Assert.assertEquals(10, players.get(3).getScoreAlteration());

        Assert.assertEquals(
                Math.abs(players.get(0).getScoreAlteration() + players.get(1).getScoreAlteration()),
                Math.abs(players.get(2).getScoreAlteration() + players.get(3).getScoreAlteration()));
    }
}
