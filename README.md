# Foosball Elo #

Foosball Elo is a balanced scoring system for table football.

This open-source Android application is based upon the Elo rating system ([https://en.wikipedia.org/wiki/Elo_rating_system](https://en.wikipedia.org/wiki/Elo_rating_system)) to calculate player ratings (skill level) and award points relative to the players competition. 
Player scores and games are saved in a local SQLite database on the device. The database consists of three tables; a table containing players, a table containing games and a relational table binding players with games.

### Elo rating? ###

Every player has a skill level (default set to 1500 points) and when one player competes against another player, the awarded (or lost) number of point depends on the other player's skill level.
Let's say that a highly skilled player competes against a low skilled player;
If the highly skilled player wins the match, he is awarded a very small amount of points and the low skilled player loses the same amount of point.
But if the low skilled player wins the match, he is awarded a large amount of point and the highly skilled player loses that amount as well.

Using this system will create a very balanced scoring system, given that enough players participate.

### How do I get it? ###

* Clone the project yourself and build it to your device.
* Get on Google Play Store: TBA.

### About ###

Author: Lars Nielsen (<lars@lndesign>)